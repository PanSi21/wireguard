#!/bin/bash
#    WireGuard VPN多用户服务端  自动配置脚本

#    本脚本(WireGuard 多用户配置)一键安装短网址
#    wget -qO- https://git.io/fpnQt | bash

#############################################################
help_info()
{
cat  <<EOF

# 一 installare wireguard su Debian 9
wget -qO- git.io/fptwc | bash

# 一 installare wireguard su Ubuntu   (源:逗比网安装笔记)
wget -qO- git.io/fpcnL | bash

# CentOS7一 su WireGuard   (https://atrandys.com/2018/886.html)
yum install -y wget && \
wget https://raw.githubusercontent.com/atrandys/wireguard/master/wireguard_install.sh \
&& chmod +x wireguard_install.sh && ./wireguard_install.sh

EOF
}
#############################################################

#定义文字颜色
Green="\033[32m"  && Red="\033[31m" && GreenBG="\033[42;37m" && RedBG="\033[41;37m" && Font="\033[0m"

#定义提示信息
Info="${Green}[信息]${Font}"  &&  OK="${Green}[OK]${Font}"  &&  Error="${Red}[错误]${Font}"

# controllare se WireGuard è installato
if [ ! -f '/usr/bin/wg' ]; then
    clear
    echo -e "${RedBG}   一键安装 WireGuard 脚本 For Debian_9 Ubuntu Centos_7   ${Font}"
    echo -e "${GreenBG}     开源项目：https://github.com/hongwenjun/vps_setup    ${Font}"
    help_info
    echo -e "${RedBG}   检测到你的vps没有正确选择脚本，请使用对应系统的脚本安装   ${Font}"
    exit 1
fi
#############################################################

# 定义修改端口号，适合已经安装WireGuard而不想改端口

# generare porta casuale
#rand(){
#    min=$1
#    max=$(($2-$min+1))
#    num=$(cat /dev/urandom | head -n 10 | cksum | awk -F ' ' '{print $1}')
#    echo $(($num%$max+$min))
#}

port=1194

mtu=1420
host=$(hostname -s)

ip_list=(16 12 21 30 32 34 36 38 40 42 48 50)

# otteniamo ip del server automaticamente
if [ ! -f '/usr/bin/curl' ]; then
    apt update && apt install -y curl
fi
serverip=$(curl -4 icanhazip.com)

# installiamo qrencode
if [ ! -f '/usr/bin/qrencode' ]; then
    apt -y install qrencode
fi

# 安装 bash wgmtu 脚本用来设置服务器
wget -O ~/wgmtu  https://raw.githubusercontent.com/hongwenjun/vps_setup/master/Wireguard/wgmtu.sh

#############################################################

# spostiamoci nella directory di wireguard
cd /etc/wireguard

# generiamo chiavi private e chiavi pubbliche per client e server
wg genkey | tee sprivatekey | wg pubkey > spublickey
wg genkey | tee cprivatekey | wg pubkey > cpublickey

# generiamo file di configurazione del server.
cat <<EOF >wg0.conf
[Interface]
PrivateKey = $(cat sprivatekey)
Address = 172.16.21.1/24
PostUp   = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -D FORWARD -o wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
ListenPort = $port
DNS = 1.1.1.1
MTU = $mtu

[Peer]
PublicKey = $(cat cpublickey)
AllowedIPs = 172.16.21.218/32

EOF

# 生成简洁的客户端配置
cat <<EOF >client.conf
[Interface]
PrivateKey = $(cat cprivatekey)
Address = 172.16.21.218/24
DNS = 1.1.1.1
#  MTU = $mtu
#  PreUp =  start   .\route\routes-up.bat
#  PostDown = start  .\route\routes-down.bat

[Peer]
PublicKey = $(cat spublickey)
Endpoint = $serverip:$port
AllowedIPs = 0.0.0.0/0, ::0/0
PersistentKeepalive = 25

EOF

# inserire combinazioni multi utente.

for i in {1..11}
do
    ip=172.16.21.${ip_list[i]}
    
    wg genkey | tee cprivatekey | wg pubkey > cpublickey

    cat <<EOF >>wg0.conf
[Peer]
PublicKey = $(cat cpublickey)
AllowedIPs = $ip/32

EOF

    cat <<EOF >wg_${host}_$i.conf
[Interface]
PrivateKey = $(cat cprivatekey)
Address = $ip/24
DNS = 1.1.1.1

[Peer]
PublicKey = $(cat spublickey)
Endpoint = $serverip:$port
AllowedIPs = 0.0.0.0/0, ::0/0
PersistentKeepalive = 25

EOF
    cat /etc/wireguard/wg_${host}_$i.conf| qrencode -o wg_${host}_$i.png
done

#  se la scheda di rete della vps non è eth0，modifichiamo con quella effettiva
ni=$(ls /sys/class/net | awk {print} | grep -e eth. -e ens. -e venet.)
if [ $ni != "eth0" ]; then
    sed -i "s/eth0/${ni}/g"  /etc/wireguard/wg0.conf
fi

# riavviamo il server wireguard
wg-quick down wg0
wg-quick up wg0
wg

conf_url=http://${serverip}:8000

cat  <<EOF > ~/wg5
 # 打包10个客户端配置，手机扫描二维码2号配置，PC使用1号配置
next() {
    printf "# %-70s\n" "-" | sed 's/\s/-/g'
}

host=$(hostname -s)

cd  /etc/wireguard/
tar cvf  wg5clients.tar  client*  wg*
cat /etc/wireguard/wg_${host}_2.conf | qrencode -o - -t UTF8
echo -e  "${GreenBG}#   手机扫描二维码2号配置，Windows 用配置请复制合适文本 ${Font}"

cat /etc/wireguard/client.conf       && next
cat /etc/wireguard/wg_${host}_2.conf   && next
cat /etc/wireguard/wg_${host}_3.conf   && next
cat /etc/wireguard/wg_${host}_4.conf   && next

echo -e "${RedBG}   一键安装 WireGuard 脚本 For Debian_9 Ubuntu Centos_7   ${Font}"
echo -e "${GreenBG}     开源项目：https://github.com/hongwenjun/vps_setup    ${Font}"
echo
echo -e "# ${Info} 使用${GreenBG} bash wg5 ${Font} 命令，可以临时网页下载配置和二维码"
echo -e "# ${Info} 使用${GreenBG} bash wgmtu ${Font} 命令，重置客户端数量，设置服务器端MTU数值或服务端口号 "

# echo -e "# ${Info} 请网页打开 ${GreenBG}${conf_url}${Font} 下载配置文件 wg5clients.tar ，${RedBG}注意: 完成后请重启VPS.${Font}"
# echo -e "#  scp root@172.16.21.1:/etc/wireguard/wg5clients.tar   wg5clients.tar"
# python -m SimpleHTTPServer 8000 &
echo ""
# echo -e "#  ${Info} 访问 ${GreenBG}${conf_url}${Font} 有惊喜， 手机扫描二维码后请立即重启VPS。"

EOF

# 显示服务器配置信息
bash ~/wg5

# 用户选择下载配置和修改mtu
sed -i "s/# python -m/python -m/g"  ~/wg5
sed -i "s/# echo -e/echo -e/g"  ~/wg5
