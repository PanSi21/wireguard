#!/bin/bash

# INSTALARE WIREGUARD SU SERVER DEBIAN.

# aggiornare i repo
apt update

# installare linux-image linux-headers
apt install linux-headers-$(uname -r) -y

# Debian9
dpkg -l|grep linux-headers

## installare WireGuard
# aggiungere i repo unstable
echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable.list
echo -e 'Package: *\nPin: release a=unstable\nPin-Priority: 150' > /etc/apt/preferences.d/limit-unstable

# ri-aggiornare i repo
apt update

# installare WireGuard e resolvconf
apt install wireguard resolvconf -y

# modprobare wireguard
modprobe wireguard && lsmod | grep wireguard

# creare le configurazioni per WireGuard

# creare cartella per le configurazioni
mkdir -p /etc/wireguard
cd /etc/wireguard

# generare le chiavi pubbliche e private
wg genkey | tee sprivatekey | wg pubkey > spublickey
wg genkey | tee cprivatekey | wg pubkey > cpublickey

# curlare l' ip del server
serverip=$(curl -4 icanhazip.com)

# 生成服务端配置文件
echo "[Interface]
# chiave privata del server
PrivateKey = $(cat sprivatekey)
# ip locale della VPN
Address = 172.16.21.1/24
# 运行 WireGuard 时要执行的 iptables 防火墙规则，用于打开NAT转发之类的。
# 如果你的服务器主网卡名称不是 eth0 ，那么请修改下面防火墙规则中最后的 eth0 为你的主网卡名称。
PostUp   = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -A FORWARD -o wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
# 停止 WireGuard 时要执行的 iptables 防火墙规则，用于关闭NAT转发之类的。
# 如果你的服务器主网卡名称不是 eth0 ，那么请修改下面防火墙规则中最后的 eth0 为你的主网卡名称。
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -D FORWARD -o wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
# porta server
ListenPort = 9999
# DNS server
DNS = 1.1.1.1
# MTU
MTU = 1420
[Peer]
# client public key
PublicKey = $(cat cpublickey)
# VPN IP client
AllowedIPs = 172.16.21.2/32" > wg0.conf

# 生成客户端配置文件
echo "[Interface]
# 私匙，自动读取上面刚刚生成的密匙内容
PrivateKey = $(cat cprivatekey)
# VPN内网IP范围
Address = 172.16.21.2/24
# DNS
DNS = 1.1.1.1
# MTU
MTU = 1300
# Wireguard客户端配置文件加入PreUp,Postdown命令调用批处理文件
PreUp = start   .\route\routes-up.bat
PostDown = start  .\route\routes-down.bat
#### 正常使用Tunsafe点击connect就会调用routes-up.bat将国内IP写进系统路由表，断开disconnect则会调用routes-down.bat删除路由表。
#### 连接成功后可上 http://ip111.cn/ 测试自己的IP。
[Peer]
# chiave pubblica del server
PublicKey = $(cat spublickey)
# ip pubblico del server e la porta configurata
Endpoint = $serverip:9009
# intervallo ip del traffico di rete
AllowedIPs = 0.0.0.0/0, ::0/0
# 保持连接，如果客户端或服务端是 NAT 网络(比如国内大多数家庭宽带没有公网IP，都是NAT)，
# 那么就需要添加这个参数定时链接服务端(单位：秒)，如果你的服务器和你本地都不是 NAT 网络，
# 那么建议不使用该参数（设置为0，或客户端配置文件中删除这行）
PersistentKeepalive = 25"|sed '/^#/d;/^\s*$/d' > client.conf

# diamo i permessi alla cartella
chmod 777 -R /etc/wireguard

sysctl_config() {
    sed -i '/net.core.default_qdisc/d' /etc/sysctl.conf
    sed -i '/net.ipv4.tcp_congestion_control/d' /etc/sysctl.conf
    echo "net.core.default_qdisc = fq" >> /etc/sysctl.conf
    echo "net.ipv4.tcp_congestion_control = bbr" >> /etc/sysctl.conf
    sysctl -p >/dev/null 2>&1
}

# apri BBR
sysctl_config
lsmod | grep bbr

# apriamo il firewall
echo 1 > /proc/sys/net/ipv4/ip_forward
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl -p

# avviamo WireGuard
wg-quick up wg0

# abilitiamo WireGuard
systemctl enable wg-quick@wg0

# verifichiamo lo stato di WireGuard
wg

# scarichiamo lo script del cinese di merda per fare qualcosa...
# DIO PORCO STO CINESE DI MERDA MI HA INCULATO!!! PORCO DI UN EPOCK!!!
# 一键 WireGuard 多用户配置共享脚本
wget -qO- https://gitlab.com/PanSi21/wireguard/raw/master/wireguard2.sh?inline=false | bash
